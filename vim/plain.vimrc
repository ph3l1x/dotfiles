"
" basic pluginless vim-config - consider it as a patch to the default-config
"
" you may finde some includes at the bottom where you can activate and
" deactivate some features
"

autocmd! bufwritepost vimrc source % " reload config if written to disk


"
" basic UI tweaks
"
set nu              " set linenumbers
syntax on           " and syntaxhighlithing
set colorcolumn=80  " colorize the 80s column
set cursorline      " set a highlight on the line where the cursor is
"set cursorcolumn    " set a hihglight on the column where the cursor is
set guicursor+=a:blinkon0  " disable cursor blinking

set showcmd
set showmatch

"
" search
"
set hlsearch        " highlight matches
set incsearch       " search in realtime


" Nice statusbar
set laststatus=2
"set statusline=
"set statusline+=%2*%-3.3n%0*\                " buffer number
"set statusline+=%f\                          " file name
"set statusline+=%h%1*%m%r%w%0*               " flags
"set statusline+=\[%{strlen(&ft)?&ft:'none'}, " filetype
"set statusline+=%{&encoding},                " encoding
"set statusline+=%{&fileformat}]              " file format
"set statusline+=%=                           " right align
"set statusline+=%2*0x%-8B\                   " current char
"set statusline+=%-14.(%l,%c%V%)\ %<%P        " offset
"
"set statusline=
"set statusline +=%1*\ %n\ %*            "buffer number
"set statusline +=%5*%{&ff}%*            "file format
"set statusline +=%3*%y%*                "file type
"set statusline +=%4*\ %<%F%*            "full path
"set statusline +=%2*%m%*                "modified flag
"set statusline +=%1*%=%5l%*             "current line
"set statusline +=%2*/%L%*               "total lines
"set statusline +=%1*%4v\ %*             "virtual column number
"set statusline +=%2*0x%04B\ %*          "character under cursor


set statusline=
set statusline+=\ #%02n\                      " buffer number
set statusline+=[%-20F]\                      " filename
"set statusline+=%-20f\                     " relative filename
set statusline+=%-3m\                       " modified-flag
set statusline+=%y\                      " file-type
set statusline+=[%-{&encoding},\                " encoding
set statusline+=%-{&fileformat}]\                " fileformat
set statusline+=[0x%02B\ @\                  " char-under-cursor-value
set statusline+=0x%02O]\                       " cursor-offset in the file
set statusline+=%=                           " sperator left <-> right
set statusline+=%l:%v\                   " virtual-column:line
set statusline+=%p%%\                          " % of file
set statusline+=\(%L\)\                         " lines total



" fancy ui stuff

" highlight trailing whitespaces
"autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
"au InsertLeave * match ExtraWhitespace /\s\+$/

set foldmethod=indent " set linefolding to indentation, but unfolds everything at opening
set foldlevel=99

" disable toolbarrs in gvim
set guioptions-=m
set guioptions-=T

"
" behavour
"
filetype plugin on " enable filetype autodetection
filetype indent on " enable indentation-detection based on filetype

set ts=4 " tabs
set sts=4
set sw=4

set expandtab " be aware of tabs
set autoindent

set visualbell  " disable nois

set scrolloff=1      " scroll one line before the screen ends
set sidescrolloff=1  " scroll one line before the screen ends

vnoremap < <gv " dont loose focus when indenting blocks
vnoremap > >gv

set bs=2    " enable backspace to delete newlines, indentations and
            " before-start-of-insert
            " see `:help bs` for details

set directory=$HOME/.vim/swapfiles// " store swapfiles here



"
" keybindigs
"
let mapleader=","  " set the map-leader

" use tabs as tabs
"map <S-h> :tabprevious<CR>  " shift-h
"map <S-l> :tabnext<CR>      " and shoft-l to travel tabs
"noremap <leader>t :tabnew<CR>

" use buffers as tabs:
" http://joshldavis.com/2014/04/05/vim-tab-madness-buffers-vs-tabs/
nmap <S-l> :bnext<CR>
nmap <S-h> :bprevious<CR>
nmap <leader>n :enew<CR>
nmap <leader>bq :bq <BAR> bd #<CR>
nmap <leader>bl :ls<CR>


noremap <leader>c :let @/ = ""<CR> " reset search term

map <c-h> <c-w>h<C-W>  " switch between panes with crtl+[hjkl]
map <c-j> <c-w>j<C-W>  " usefull for NERDTree, python-mod, fungitiv, etc
map <c-k> <c-w>k<C-W>
map <c-l> <c-w>l<C-W>

noremap <leader>e :q<CR> " they explain themself
noremap <leader>s :w<CR>
noremap <leader>x :x<CR>
noremap <leader>b :w<CR>:make<CR>

vmap Q gq   " reformat
nmap Q gqap

map <leader>l :set list!<cr>
map <leader>i :set paste!<cr>
map <leader>R :set relativenumber!<cr>


" mouse support
set mouse=a " enable it

" enable word highlighting on doubleclick
map <2-LeftMouse> :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>
imap <2-LeftMouse> :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>

"
" commands
"

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %


"
" colors
"
set background=dark
colorscheme elflord
"colorscheme pablo
"colorscheme base16-3024

if has("gui_running")
    colorscheme solarized
    "colorscheme macvim
    "colorscheme base16-3024
endif


set guifont=monofur:h14
set antialias

" small and clear
"set guifont=terminus\ (TTF):h12
"set noantialias

" show whitespace-charaters
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
"set listchars=eol:$,tab:↹,trail:~,extends:>,precedes:<
"set listchars=eol:\¬,tab:\▸-,trail:~,extends:>,precedes:<
"set listchars=eol:\↵,tab:\▸▸,space:\∙,trail:⌧
"set listchars=eol:\↵,tab:\▸▸,space:\∙,trail:⌧

" enable switching of unsaved buffers
set hidden
