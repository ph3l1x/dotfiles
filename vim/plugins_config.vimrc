" NERDTREE
"map <leader>, :NERDTreeTabsToggle<CR>
map <leader>, :TagbarToggle<CR>
"map <leader>, :NERDTreeToggle<CR>

let g:nerdtree_tabs_open_on_console_startup=0 " open the NERDTree on vim startup (means always)
let g:nerdtree_tabs_open_on_gui_startup=0


" TAGBAR
map <leader>v :NERDTreeTabsToggle<CR>
"let g:tagbar_width = 31
"let g:tagbar_left = 1


" cycles through NERDTree, Tagbar, None with <leader>,
function NTToggle()
    let w:jumpbacktohere = 1

    if exists('t:NERDTreeBufName')
        let nerdtree_open = bufwinnr(t:NERDTreeBufName) != -1
    else
        let nerdtree_open = 0
    endif
    let tagbar_open = bufwinnr('__Tagbar__') != -1


    if !nerdtree_open && !tagbar_open
        NERDTreeTabsOpen
    elseif nerdtree_open
        NERDTreeTabsClose
        TagbarOpen
    elseif tagbar_open
        TagbarClose
    endif

    " Jump back to the original window
    for window in range(1, winnr('$'))
        execute window . 'wincmd w'
        if exists('w:jumpbacktohere')
            unlet w:jumpbacktohere
            break
        endif
    endfor
endfunction
"map <leader>,   :call NTToggle()<cr>


" PYMODE
let g:pymode_virtualenv = 1
let g:pymode_rope_complete_on_dot = 0
let g:pymode_rope_completion = 0
let g:pymode_rope_autoimport = 0
let g:pymode_rope = 0  "disable rope completly


" CTRLP
" use the neares .git, .hg and some others as cwd
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/](\.(git|hg|svn)|\_site)$',
    \ 'file': '\v\.(exe|so|dll|class|png|jpg|jpeg)$',
\}
let g:ctrlp_working_path_mode = 'r'
nmap <leader>p :CtrlP<CR>
nmap <leader>bb :CtrlPBuff<CR>
nmap <leader>bm :CtrlPMixed<CR>
nmap <leader>bs :CtrlPMRU<CR>
nmap <leader>o  :CtrlP .<CR>


" VIMUX
map <leader>m :wa<cr>:VimuxRunLastCommand<CR>
map <leader>M :wa<cr>:VimuxRunCommand("")
"map <leader>M :VimuxRunCommand("$(fc -ln -1)")<CR>


" AIRLINE
" enable bufferlist
let g:airline#extensions#tabline#enabled = 0
" show filenames only
"let g:airline#extensions#tabline#fnamemod = ':t'

" BUFFERLINE
let g:bufferline_echo = 1
let g:bufferline_show_bufnr = 0
 
"MINIBUFEXPLORER
nmap <leader>mr :MBEClose<CR>:MBEOpen<CR>
nmap <leader>mc :MBEClose<CR>
nmap <leader>mo :MBEOpen<CR>

" RESET ALL WINDOWS

function WindowToggle()
    NERDTreeTabsClose
    TagbarClose
    MBEClose

    NERDTreeTabsOpen
    TagbarOpen
    MBEOpen
endfunction

nmap <leader>r :call WindowToggle()<cr>


" puppet @ tagbar
" https://gist.github.com/trlinkin/6924042
"--- In your ~/.vimrc

let g:tagbar_type_puppet = {
  \ 'ctagstype': 'puppet',
  \ 'kinds': [
    \'c:classes',
    \'s:sites',
    \'n:nodes',  
    \'d:definitions',
    \'r:resources',
    \'f:default',
    \'v:variables'
  \]
\}
