set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required

Plugin 'gmarik/Vundle.vim'
Plugin 'nelstrom/vim-markdown-folding'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
"Plugin 'ervandew/supertab'
"Plugin 'scrooloose/syntastic'
Plugin 'majutsushi/tagbar'
"Plugin 'altercation/vim-colors-solarized'
Plugin 'terryma/vim-multiple-cursors'
"Plugin 'tpope/vim-fugitive'
"Plugin 'chriskempson/base16-vim'
"Plugin 'tpope/vim-markdown'
"Plugin 'ludovicchabant/vim-lawrencium'
"Plugin 'Rip-Rip/clang_complete.git'
"Plugin 'benmills/vimux'
"Plugin 'klen/python-mode'
Plugin 'kien/ctrlp.vim'
"Plugin 'jceb/vim-orgmode'
"Plugin 'tpope/vim-speeddating'
Plugin 'DrawIt'
"Plugin 'kshenoy/vim-signature'
"Plugin 'ironcamel/vimchat'
"Plugin 'CyCoreSystems/vim-cisco-ios'
"Plugin 'vim-airline/vim-airline'
"Plugin 'bling/vim-bufferline'
Plugin 'fholgado/minibufexpl.vim'
"Plugin 'MarcWeber/vim-addon-mw-utils'
"Plugin 'tomtom/tlib_vim'
"Plugin 'garbas/vim-snipmate'
"Plugin 'honza/vim-snippets'
"Plugin 'guyzmo/vim-etherpad'
"Plugin 'xolox/vim-misc'
"Plugin 'xolox/vim-notes'
"Plugin 'puppetlabs/puppet-syntax-vim'
"Plugin 'lpenz/vimcommander'
"Plugin 'severin-lemaignan/vim-minimap'
"Plugin 'Valloric/YouCompleteMe'
"Plugin ''

" disabled plugins
" to install uncomment and run :PluginInstall
" to remove comment and run :PluginClean
"Plugin 'scrooloose/nerdtree-git-plugin'
"Plugin 'Lokaltog/vim-powerline'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
