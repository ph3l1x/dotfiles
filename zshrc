#
# SHORTCUTS, ALIASES, ETC
#


# usefull default options
alias ls="ls -F --color=auto"
alias la="ls -a"
alias grep="grep --color=auto"
alias fgrep="fgrep --color=auto"
alias vi="vim"


# configless tools
alias null-irssi='irssi --config=/dev/null'
alias null-mutt='mutt -n -f /dev/null -F /dev/null'
alias null-screen='screen -c /dev/null'
alias null-tmux='tmux -f /dev/null'
alias null-vim='vim -u NONE'
alias null-zsh='zsh -f'

# setf:zsh
#
# ENV
#

export EDITOR=vim
export PAGER=less
export BROWSER=links
export VISUAL="${EDITOR}"

export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"

export UMASK=022

# used by mutt for pinentry
export GPG_TTY=$(tty)

HISTSIZE=9999
HISTFILE=~/.zsh_history
SAVEHIST=9999

test -d $HOME/bin && PATH=$PATH:$HOME/bin


# setf:zsh
#
# BEHAVIOUR of our shell
#

# Report the status of background jobs immediately, rather than
# waiting until just before printing a prompt.
setopt notify

# Report the status of background and suspended jobs before exiting a shell
# with job control; a second attempt to exit the shell will succeed.
setopt checkjobs

# Send *not* a HUP signal to running jobs when the shell exits.
setopt nohup

# access a programms executable via equals sign (file =ls)
setopt equals

# Do query the user before executing 'rm *' or 'rm path/*'
# $ rm -rf *
# zsh: sure you want to delete all the files in /home/dope/foo [yn]?
setopt normstarsilent

# Shut up ;)
setopt nobeep

# Allow the short forms of for, select, if, and function constructs, i.
# e.: ``for i (*.o) rm $i'' instead of ``for i in *.o; do rm $i; done''
setopt shortloops

setopt auto_pushd

DIRSTACKSIZE=25

setopt autopushd pushdsilent pushdtohome

## Remove duplicate entries
setopt pushdignoredups

## This reverts the +/- operators.
setopt pushdminus

#TMOUT=120
#TRAPALRM() {cmatrix -sabu2}

#
#
# setf:zsh
#
# COMPLETION
#

# Initialisation for new style completion.
if [[ $ZSH_VERSION  > '4.2' ]]; then
	autoload -U compinit
	compinit
else
	print "Advanced completion system not found; ignoring zstyle settings."
	function zstyle { }
fi

#
# BEHAVIOUR
#

# If unset, the cursor is set to the end of the word if completion is
# # started. Otherwise it stays there and completion is done from both ends.
setopt completeinword

#  When listing files that are possible completions, show the
# type of each file with a trailing identifying mark.
setopt list_types

#  Do not require a leading '.' in a filename to be matched explicitly.
setopt globdots

# highlight matching part of available completions
autoload -U colors ; colors
zstyle ':completion:*' list-colors  'reply=( "=(#b)(*$PREFIX)(?)*=00=$color[green]=$color[bg-green]" )'

# $ man write<TAB>
# manual page, section 1:
# write
# manual page, section 1p:
# write
# ...
# Its evil. Isn't it?!
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:descriptions' format $'%{\e[0;31m%}completing %B%d%b%{\e[0m%}'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format $'%{\e[0;31m%}No matches for:%{\e[0m%} %d'
zstyle ':completion:*' format $'%{\e[0;31m%}completing %B%d%b%{\e[0m%}'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' verbose yes
zstyle ':completion:*' file-sort name

# how many completions switch on menu selection
# use 'long' to start menu compl. if list is bigger than screen
# or some number to start menu compl. if list has that number
# of completions (or more).
zstyle ':completion:*' menu select=long

# If there are more than 5 options, allow selecting from a menu with
# arrows (case insensitive completion!).
zstyle ':completion:*-case' menu select=5

# Messages/warnings format
zstyle ':completion:*:messages' format $'%{\e[0;31m%}%d%{\e[0m%}'
zstyle ':completion:*:warnings' format $'%{\e[0;31m%}No matches for: %d%{\e[0m%}'

# zstyle ':completion:*' menu select
# zstyle ':completion:*:default'         list-colors ${(s.:.)LS_COLORS}


# Limit this fuckung "zsh: do you wish to see all NNN possibilities (NNN
# lines)?" downward (default is 100). Only ask before displaying
# completions if doing so would scroll.
LISTMAX=0

# setf zsh
#
# THEME
#
# this file contains promt related stuff

# Executed whenever a command has a non-zero exit status:
if fortune bofh-excuses > /dev/null 2>&1 ;then
    TRAPZERR() { echo -n $?': '; fortune bofh-excuses | tail -n 1 }
else
	TRAPZERR() { echo $?': AAAAAAAARRRRRGHHHHH!!'; }
fi

# use colors
autoload -U colors && colors


## a simple prompt
#export PS1="%0(?,,%? )%B%n@%m %5c %#%b "
#export RPROMPT="%K{black}%F{yellow} %B%3c%b%K{black}%F{yellow} %T (%y) %f%k"
#export PS1="%B%0(?,>,%? !) %b"
#export RPROMPT="%B%3c %n@%m %T (%y) %i %b"
#export PS1="%{%K{green}%F{black}%} %0(?,>,%? !) %{%f%k%} "
#export RPROMPT="%{%K{green}%F{black}%} %3c %n@%m %T (%y) %i %{%f%k%}"
#export PS1="%K{black} %0(?,,(%F{red}%?%f%) )%F{white}%n@%m%f %5c %F{red}%f$%k "
export PS1=" %0(?,,(%?%) )%B>%b "
#export RPROMPT="%0(?,,%B%?%b) %T %y %i"
export RPROMPT="%B %5c %n%b@%B%m%f%b %y:%i %1(j,(%j jobs%) ,)%T "

#function zle-line-init zle-keymap-select {
#    RPS1="${${KEYMAP/vicmd/-- NORMAL --}/(main|viins)/-- INSERT --}"
#    RPS2=$RPS1
#    zle reset-prompt
#}

#zle -N zle-line-init
#zle -N zle-keymap-select

# format of process time reports with 'time'
#  %%     A `%'.
#  %U     CPU seconds spent in user mode.
#  %S     CPU seconds spent in kernel mode.
#  %E     Elapsed time in seconds.
#  %P     The CPU percentage, computed as  (%U+%S)/%E.
#  %J     The name of this job.
# Default is:
#       %E real  %U user  %S system   %P  %J
# TIMEFMT="\
#     The name of this job.             : %J
#     CPU seconds spent in user mode.   : %U
#     CPU seconds spent in kernel mode. : %S
#     Elapsed time in seconds.          : %E
#     The  CPU percentage.              : %P"

TIMEFMT="\
line: %J
time: %E
    user:   %U
    system: %S
cpu : %P
mem : %K, <= %M (major: %F, minor: %R pagefaults)
I/O : %I/%O
CS: expir: %c, wait: %W"

# If nonnegative, commands whose combined user and system execution
# times (measured in seconds) are greater than this value have timing
# # statistics printed for them.
REPORTTIME=60


# dirhashes, see 06_shortcuts
#hashdir D   $HOME/Downloads/
#hashdir M   $HOME/Maildir/
#hashdir MS  $HOME/Maildir/Save/



# setf:zsh:w
#
# FUNCTION
#

# show new files
new () {ls -lt $@ | head}

# mount fs
m() { 
    local sudo;
    test `id -u` -ne 0 && sudo="sudo";
    if [ ! -d /mnt/$1 ]; then $sudo mkdir -p /mnt/$1; fi
    $sudo mount /dev/$1 /mnt/$1 && cd /mnt/$1 || echo "cannot enter /mnt/$1"}

# mount crypted fs
mc() {
    local sudo;
    test `id -u` -ne 0 && sudo="sudo";
    $sudo cryptsetup luksOpen /dev/$1 $1 && m mapper/$1 }

# unmount crypted fs
mcu() {
    local sudo;
    test `id -u` -ne 0 && sudo="sudo";
    $sudo umount -R /mnt/mapper/$1 && $sudo cryptsetup luksClose $1 }


# setf:zsh
#
# KEYBINDINGS
#


# hostory related
bindkey "^[[A" history-search-backward	# PgUp
bindkey "[B" history-search-forward	# PgDown
# Whenever the user enters a line with history expansion,
# don't execute the line directly; instead, perform
# history expansion and reload the line into the editing buffer.
setopt hist_verify

bindkey "^R" history-incremental-search-backward


# save and clean the line, reload the saved one after return
bindkey "q" push-line			# Kill the *complete* line! (ESC+q)


# let ctrl+n prefix the current line with `sudo`
run-with-sudo () { LBUFFER="sudo $LBUFFER" }
zle -N run-with-sudo
bindkey '^N' run-with-sudo


# let ctrl+w surround the current line with a loop
run-with-while () { LBUFFER="while [ 1 ]; do $LBUFFER; sleep 1; done" }
zle -N run-with-while
bindkey '^W' run-with-while


# take the first word in the line an prepend it with `man`
manualize () { LBUFFER="man `echo $LBUFFER | sed 's/ .*//g'`" }
zle -N manualize
bindkey '^B' manualize

while1 () { LBUFFER="while [ 1 ]; do $LBUFFER; sleep 10; done" }
zle -N while1
bindkey '^Y' while1

# prints dir-history, you need to press return afterwards
#dirhist () { OLDBUFFER=$LBUFFER; LBUFFER="dirs -v"; }
#dirhist () { zle push-line; echo;dirs -v;}
#zle -N dirhist
#bindkey '^H' dirhist


#TODO check if this is nesscesarry
# fix Home and End buttons
bindkey ${terminfo[khome]} beginning-of-line
bindkey ${terminfo[kend]} end-of-line
bindkey ${terminfo[kdch1]} delete-char

# enable vimmode
bindkey -v

# setf:zsh
#
# iab
#

setopt extendedglob
typeset -Ag abbreviations

# the abbrevation themself
abbreviations=(
  "Adate"	"\`date +%y-%m-%d\`"
  "Atime"	"\`date +%y-%m-%d_%H:%M:%S\`"
  "Rdate"	"\`date +%y%m%d\`"
  "Rtime"	"\`date +%y%m%d%H%M%S\`"
)

# all the magic making them warking
magic-abbrev-expand() {
    local MATCH
    LBUFFER=${LBUFFER%%(#m)[_a-zA-Z0-9]#}
    LBUFFER+=${abbreviations[$MATCH]:-$MATCH}
    zle self-insert
}

no-magic-abbrev-expand() {
  LBUFFER+=' '
}

zle -N magic-abbrev-expand
zle -N no-magic-abbrev-expand
bindkey " " magic-abbrev-expand
bindkey "^x " no-magic-abbrev-expand
bindkey -M isearch " " self-insert


#
# ################# PROMPT ####################
#
#""""""""""""""""""""""""""""""""""""""""""""""""""


setopt prompt_subst


function precmd {

    local PR_FILL_PATTERN="─"
    local PR_OVERSIZE=4

    local PR_BOTTOMTEXT_MASK="( %? - `fc -lnD -1` )"
    # crazy variable-expansion ahead!
    local BOTTOMTEXT_LENGHT=$(( ${#${(%):-$PR_BOTTOMTEXT_MASK}} + $PR_OVERSIZE ))
    local BOTTOM_FILL_LENGTH=$(( $COLUMNS - $BOTTOMTEXT_LENGHT))
    local BOTTOM_FILL_LENGTH_LEFT=$(( $BOTTOM_FILL_LENGTH / 2 ))
    local BOTTOM_FILL_LENGTH_RIGHT=$(( $BOTTOM_FILL_LENGTH / 2 + $BOTTOM_FILL_LENGTH % 2))
    local PR_BOTTOM_LEFT="\${(l.${BOTTOM_FILL_LENGTH_LEFT}..${PR_FILL_PATTERN}.)}"
    local PR_BOTTOM_RIGHT="\${(l.${BOTTOM_FILL_LENGTH_RIGHT}..${PR_FILL_PATTERN}.)}"

    local PR_TOPTEXT_MASK="( %5d )"
    # crazy variable-expansion ahead!
    local TOPTEXT_LENGTH=$(( ${#${(%):-$PR_TOPTEXT_MASK}} + $PR_OVERSIZE ))
    local TOP_FILL_LENGTH=$(( $COLUMNS - $TOPTEXT_LENGTH))
    local TOP_FILL_LENGTH_LEFT=$(( $TOP_FILL_LENGTH / 2 ))
    local TOP_FILL_LENGTH_RIGHT=$(( $TOP_FILL_LENGTH / 2 + $TOP_FILL_LENGTH % 2))
    local PR_TOP_LEFT="\${(l.${TOP_FILL_LENGTH_LEFT}..${PR_FILL_PATTERN}.)}"
    local PR_TOP_RIGHT="\${(l.${TOP_FILL_LENGTH_RIGHT}..${PR_FILL_PATTERN}.)}"


    export PS1="%B ╰$PR_BOTTOM_LEFT( %0(?,%F{green},%F{red})$? - `fc -lnD -1`%f )$PR_BOTTOM_RIGHT╯

 ╭$PR_TOP_LEFT( %5d )$PR_TOP_RIGHT╮ 
 ╞═( %n%b@%B%m ) %%%b "

    export RPROMPT="%B( %L:%y:%j %T )═╡%b"

}


#""""""""""""""""""""""""""""""""""""""""""""""""""

# setf:zsh
